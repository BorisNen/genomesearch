**Using the project without compiling the source code:**
1. Go to the "packaging" folder and unzip the contents of "main_window.zip".
2. Inside the unzipped contents there will be a "main_window.exe" - double click on it.
3. Any that is not logged inside the log window will appear in the cmd window.

**Last tested stable dependencies:**

| Library       | Version   |
| ------        | ------    |
|Pillow	        |7.1.2      |
|certifi	    |2020.4.5.2 |
|chardet	    |3.0.4      |
|idna	        |2.9        |
|joblib	        |0.15.1     |
|numpy	        |1.18.5     |
|pip	        |20.1.1     |
|requests	    |2.23.0     |
|scikit-learn   |0.23.1     |
|scipy	        |1.4.1	    |
|setuptools	    |47.1.1	    |
|threadpoolctl	|2.1.0	    |
|ttkthemes	    |3.0.0	    |
|urllib3	    |1.25.9	    |

**Classifying:**

1. Download necessary data that is split into different classes.
	Fasta files should contain data that starts with >lcl.
	All instances of the dataset should be in one file, before each >lcl there should be a blank line and there should be 2 blank lines at the end of the file.
2. Split the data into test and train files.
		The test file should contain 30% of each class from the train file.
		The train file should contain the rest.
		The classes need to be ordered into the files as follows:
		All instances from one class should be grouped first. Followed by all instances of the second class. 
		And so on until all classes of the same type are goruped together.
		Example

		File:
			-Instance of Class A
			-Instance of Class A
			-Instance of Class A
			...
			-Instance of Class B
			-Instance of Class B
			...
			-Instance of Class C
			
			End of file
			
		Note the number of instances of each class.

3. Go to the machine learning section.
4. Click specify classes and enter the label of each class and then the numbers of all instances for it.
		Example:
		Label A - 300
		Label B - 170
		Label C - 60
5. Click "Generate new test set"
		Point to the direction of the file, containing all test examples in fasta format.
		Choose where to save it and with what name
		Finish the process with "Generate"
6. Now choose the newly created .npy file from step 5.
		Choose where to save it
		Choose number of components for PCA compression (It should be the same as the files that are going to be classified!!)
7. Before training the model - configure it (optional)
	If you want to modify the number of layers, neurons, actication function of backpropagation function -> go to Configure MLP
	Choose the necessary options and click "Save and Exit"
7. Go to Train MLP
	Choose the .npy file from step 6 that was compressed with PCA
	Choose where to save it
	Choose a name for it and click Train
	A confirmation message should appear in the log
8. Classifying options:
	Classify uknown data:
		Go through steps 5 and 6 (with the specified classes from the trained model)
	Classify with MLP:
		Go through steps 4, 6 and 6 (specify class labels for the correct predictions)
9. Using an already saved model
	- PCA components number should be the same for the model and the data that needs to be classified.
		Otherwise, the length of each sequence might differ and the model will not be able to make a prediction.
	- Specifying the classes for the model in case of unknown data and specifying the classes for the correct predictions of the known data need to be done everytime the program is launched!!!!
	
**Quick start guide:**

	How to train a model?
		1. Load data_values.txt from the resources folder into the text area.
		2. Choose specify classes and write the HLA_ALL_TRAIN data from the file in step 1.
		3. Generate new test set with the new file from step 2 without inputing the longest sequence (It will be shown in the log after generation).
		4. Perform PCA with the new file from step 3. Note the number of components for use in the test set.
		5. Configure MLP (Optional)
		6. Choose the "Train MLP" radiobutton and train a model with the file from step 5.
		
	How to predict?
		With PCA and known data:
			1. Specify classes, input the HLA_ALL_TEST data from data_values.txt
			2. Generate new test set with the file from step 1 without inputing the longest sequence (It will be shown in the log after generation).
			3. Perform PCA with the new file from step 3 and use the same components as with the training data in the model.
			4. Configure MLP (Optional)
			5. Choose "Classify with MLP" radiobutton and choose the trained model and the file from step 3.
		Without PCA and with unknown data:
			1. Train a new model by skipping step 4 in the "How to train a model" guide.
			2. Specify the classes that were in the trained model (count field is not important when classyfing, but fill it correctly).
			3. Generate a new test set with the unknown data and specify the longest sequence of the trained model in the popup window.
			4. Choose "Classify uknown data" radiobutton, select the model from step 1, select the file from step 3 and classify.
			5. Note that the modal can only label classes that it already contains!!