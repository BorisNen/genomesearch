"""
GenomeSearch
    Copyright (C) 2020  BorisNen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
# ======================
# imports
# ======================
import tkinter as tk
from tkinter import ttk, Menu
from ttkthemes import themed_tk as themetk
from sys import exit
from PIL import ImageTk, Image

import genomeSearch.gui.popup as popup
import genomeSearch.resources as resources
import genomeSearch.gui.navigation_window as navigation
import genomeSearch.gui.editor_window as editor
import genomeSearch.gui.tools_window as tools
import genomeSearch.gui.log_window as log
import genomeSearch.gui.messagebox as msg
import genomeSearch.databaseRequests.entrez_query as entrez

# ======================
# Global Constants
# ======================
LANGUAGES = [
    ("English", 'resources\\English.properties'),
    ("Български", 'resources\\Bulgarian.properties'),
    ("Deutsch", 'resources\\German.properties'),
]
INSTANCES = {}


class GUI:
    def __init__(self):
        # Create instance
        #self.win = tk.Tk()
        self.win = themetk.ThemedTk()
        self.win.geometry('900x600')
        self.win.set_theme("radiance")

        # w = self.win.winfo_screenwidth()
        # h = self.win.winfo_screenheight()

        # self.win.geometry("%dx%d+0+0" % (w, h))

        # Fill in window
        self.load_menu_icons()
        self.main_window_content()
        self.load_labels()

        self.ask_language()

    def ask_language(self):
        self.instance = popup.Popup('320x200', resources.get_labels('Popup', 'option'))

        window = self.instance.return_popup()
        col = 0
        self.var = tk.StringVar()
        for text, lang in LANGUAGES:
            radio_button = ttk.Radiobutton(window, text=text, variable=self.var, value=lang,
                                           command=self._language_choice)
            radio_button.grid(column=0, row=col, sticky=tk.W, padx=10, pady=10)
            col += 1

    def load_labels(self):
        # Add a title
        self.win.title(resources.get_labels('MainWindow', 'title'))
        self.load_menu()
        self.navigation_instance.load_labels()
        self.editor_instance.get_current_text_area().right_click_menu()
        self.tools_instance.load_labels()

    def load_menu_icons(self):
        self.new_img = ImageTk.PhotoImage(Image.open(resources.get_icons("resources\icons\\new_file.ico")))
        self.language_img = ImageTk.PhotoImage(Image.open(resources.get_icons("resources\icons\language.ico")))
        self.open_img = ImageTk.PhotoImage(Image.open(resources.get_icons("resources\icons\open.ico")))
        self.save_img = ImageTk.PhotoImage(Image.open(resources.get_icons("resources\icons\save.ico")))
        self.save_as_img = ImageTk.PhotoImage(Image.open(resources.get_icons("resources\icons\save_as_icon.ico")))
        self.cut_img = ImageTk.PhotoImage(Image.open(resources.get_icons("resources\icons\cut.ico")))
        self.copy_img = ImageTk.PhotoImage(Image.open(resources.get_icons("resources\icons\copy.ico")))
        self.paste_img = ImageTk.PhotoImage(Image.open(resources.get_icons("resources\icons\paste.ico")))
        self.undo_img = ImageTk.PhotoImage(Image.open(resources.get_icons("resources\icons\\undo.ico")))
        self.redo_img = ImageTk.PhotoImage(Image.open(resources.get_icons("resources\icons\\redo.ico")))
        self.remote_img = ImageTk.PhotoImage(Image.open(resources.get_icons("resources\icons\\remote.ico")))
        self.about_img = ImageTk.PhotoImage(Image.open(resources.get_icons("resources\icons\\about.ico")))

    def load_menu(self):
        # Creating a Menu Bar
        menu_bar = Menu(self.win)
        self.win.config(menu=menu_bar)

        # Create menu and add menu items
        file_menu = Menu(menu_bar, tearoff=0)
        file_menu.add_command(label=resources.get_labels('MainMenu', 'new'), accelerator='Ctrl+N', compound='left',
                              image=self.new_img, underline=0, command=self._editor_tab)
        file_menu.add_command(label=resources.get_labels('MainMenu', 'open'), accelerator='Ctrl+O', compound='left',
                              image=self.open_img, underline=0, command=self.editor_instance.open_file_dialog)
        file_menu.add_command(label=resources.get_labels('MainMenu', 'save'), accelerator='Ctrl+S', compound='left',
                              image=self.save_img, underline=0, command=self.editor_instance.get_current_text_area().save)
        file_menu.add_command(label=resources.get_labels('MainMenu', 'save_as'), accelerator='Ctrl+Shift+S', compound='left',
                              image=self.save_as_img, underline=0, command=self.editor_instance.get_current_text_area().save_as)

        file_menu.add_separator()

        file_menu.add_command(label=resources.get_labels('MainMenu', 'access'), compound='left',
                              image=self.remote_img, underline=0, command=self.entrez_query)
        file_menu.add_separator()

        file_menu.add_command(label=resources.get_labels('MainMenu', 'exit'), command=self._quit)
        menu_bar.add_cascade(label=resources.get_labels('MainMenu', 'file'), menu=file_menu)
        # add_cascade aligns the menu items one below another vertically

        # Add edit Menu to the Menu Bar and items
        edit_menu = Menu(menu_bar, tearoff=0)
        menu_bar.add_cascade(label=resources.get_labels('MainMenu', 'edit'), menu=edit_menu)
        edit_menu.add_command(label=resources.get_labels('Editor', 'undo'), accelerator='Ctrl+Z', compound='left',
                              image=self.undo_img, underline=0, command=self.editor_instance.get_current_text_area().undo)
        edit_menu.add_command(label=resources.get_labels('Editor', 'redo'), accelerator='Ctrl+Y', compound='left',
                              image=self.redo_img, underline=0, command=self.editor_instance.get_current_text_area().redo)

        edit_menu.add_separator()

        edit_menu.add_command(label=resources.get_labels('Editor', 'cut'), accelerator='Ctrl+X', compound='left',
                              image=self.cut_img, underline=0, command=self.editor_instance.get_current_text_area().cut)
        edit_menu.add_command(label=resources.get_labels('Editor', 'copy'), accelerator='Ctrl+C', compound='left',
                              image=self.copy_img, underline=0, command=self.editor_instance.get_current_text_area().copy)
        edit_menu.add_command(label=resources.get_labels('Editor', 'paste'), accelerator='Ctrl+V', compound='left',
                              image=self.paste_img, underline=0, command=self.editor_instance.get_current_text_area().paste)

        edit_menu.add_separator()

        edit_menu.add_command(label=resources.get_labels('Editor', 'find'), accelerator='Ctrl+F',
                              command=self.editor_instance.get_current_text_area().find_text)
        edit_menu.add_separator()
        edit_menu.add_command(label=resources.get_labels('Editor', 'select_all'), accelerator='Ctrl+A',
                              command=self.editor_instance.get_current_text_area().select_all)

        # Add a view Menu to the Menu Bar and items
        view_menu = Menu(menu_bar, tearoff=0)
        menu_bar.add_cascade(label=resources.get_labels('MainMenu', 'view'), menu=view_menu)

        view_menu.add_checkbutton(
            label=resources.get_labels('MainMenu', 'cursor_tracker'), variable=editor.show_cursor_info, command=self.editor_instance.show_cursor_info_bar)

        # Add help Menu to the Menu Bar and help item
        help_menu = Menu(menu_bar, tearoff=0)
        menu_bar.add_cascade(label=resources.get_labels('MainMenu', 'help'), menu=help_menu)
        help_menu.add_command(label=resources.get_labels('MainMenu', 'about'), compound='left',
                              image=self.about_img, underline=0, command=self.about)

        # Add language to the Menu Bar and language item
        lang_menu = Menu(menu_bar, tearoff=0)
        menu_bar.add_cascade(label=resources.get_labels('MainWindow', 'language'), menu=lang_menu)
        lang_menu.add_command(label=resources.get_labels('MainMenu', 'choose_language'), compound='left',
                              image=self.language_img, underline=0, command=self.ask_language)

    def main_window_content(self):
        logo = resources.get_icons("resources\icons\Logo.ico")
        self.win.wm_iconbitmap(bitmap=logo)

        # Instantiate horizontal resizable panel widget container
        row_container = ttk.PanedWindow(self.win, orient=tk.VERTICAL)
        row_container.pack(fill=tk.BOTH, expand=True)

        # Instantiate vertical resizable panel widget container
        column_container = ttk.PanedWindow(row_container, orient=tk.HORIZONTAL)
        column_container.grid(column=0, row=1, columnspan=3, padx=1, pady=1, sticky=tk.NSEW)
        column_container.rowconfigure(0, weight=1)
        column_container.columnconfigure(0, weight=1)

        # Creating the four windows
        # Instance for messaging and feedback
        log_frame = tk.Frame(row_container, relief=tk.SUNKEN)
        log_frame.grid(column=0, row=1, columnspan=3, padx=1, pady=1, sticky=tk.NSEW)
        log_frame.rowconfigure(0, weight=6)
        log_frame.columnconfigure(0, weight=1)
        self.log_instance = log.Log(log_frame)
        INSTANCES.__setitem__('log_instance', self.log_instance)

        # Instance of the text display and editing area
        editor_frame = tk.Frame(column_container, relief=tk.SUNKEN)
        editor_frame.grid(column=1, row=0, padx=1, pady=1, sticky=tk.NSEW)
        editor_frame.rowconfigure(0, weight=1)
        editor_frame.columnconfigure(0, weight=6)
        self.editor_instance = editor.Editor(editor_frame, self)
        INSTANCES.__setitem__('editor_instance', self.editor_instance)

        # Instance of the folder navigation area
        navigation_frame = tk.Frame(column_container, relief=tk.SUNKEN)
        navigation_frame.grid(column=0, row=0, padx=1, pady=1, sticky=tk.NSEW)
        navigation_frame.rowconfigure(0, weight=6)
        navigation_frame.columnconfigure(0, weight=1)
        self.navigation_instance = navigation.Navigation(navigation_frame, self)
        INSTANCES.__setitem__('navigation_instance', self.navigation_instance)

        # Instance with all icons and actions
        tools_frame = tk.Frame(column_container, relief=tk.SUNKEN)
        tools_frame.grid(column=2, row=0, padx=1, pady=1, sticky=tk.NSEW)
        tools_frame.rowconfigure(0, weight=6)
        tools_frame.columnconfigure(0, weight=1)
        self.tools_instance = tools.Tools(tools_frame, self)
        INSTANCES.__setitem__('tools_instance', self.tools_instance)

        # Add all instances to their resizable containers
        column_container.add(navigation_frame, weight=1)
        column_container.add(editor_frame, weight=8)
        column_container.add(tools_frame, weight=1)
        row_container.add(column_container, weight=4)
        row_container.add(log_frame, weight=1)

    def entrez_query(self):
        entrez.Entrez(self)

    def about(self):
        msg.info(resources.get_labels('MessageBox', 'msg_title_info'),
                 resources.get_labels('About', 'info'))

    def return_class_instance(self, class_name):
        return INSTANCES.get(class_name)

    def _editor_tab(self):
        self.editor_instance.confirm_add()

    # Radiobutton Callback
    def _language_choice(self):
        resources.SELECTED_LANGUAGE = self.var.get()
        self.load_labels()
        # self.win.deiconify()
        self.instance.destroy()

    # Exit GUI cleanly
    def _quit(self):
        self.win.quit()
        self.win.destroy()
        exit()


# ======================
# Start gui
# ======================
gui = GUI()
gui.win.mainloop()
