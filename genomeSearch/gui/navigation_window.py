"""
GenomeSearch
    Copyright (C) 2020  BorisNen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
# ======================
# imports
# ======================
import tkinter as tk
import glob, os
from tkinter import ttk
import genomeSearch.resources as resources
import genomeSearch.parsers.entrez_parse as parser

# ======================
# Global Constants
# ======================
ALLOWED_EXTENSIONS = ['.xml', '.txt', '.fasta']
INITIAL_PATH = None


class Navigation:
    def __init__(self, parent, main_instance):
        self.main_instance = main_instance
        self.vsb = ttk.Scrollbar(parent, orient="vertical")
        self.hsb = ttk.Scrollbar(parent, orient="horizontal")

        self.tree = ttk.Treeview(parent, columns=("fullpath", "type", "size"),
                            displaycolumns="size", yscrollcommand=lambda f, l: self.autoscroll(self.vsb, f, l),
                            xscrollcommand=lambda f, l: self.autoscroll(self.hsb, f, l))

        self.vsb['command'] = self.tree.yview
        self.hsb['command'] = self.tree.xview

        self.load_labels()
        self.tree.column("size", stretch=True)

        self.populate_roots(self.tree)
        self.tree.bind('<<TreeviewOpen>>', self.update_tree)
        self.tree.bind('<Double-Button-1>', self.change_dir)

        # self.tree.pack(fill=tk.BOTH, expand=True, side="left")
        # self.vsb.pack(fill=tk.BOTH, expand=True, side="right")
        # self.hsb.pack(fill=tk.BOTH, expand=True, side="bottom")

        # Arrange the tree and its scrollbars in the toplevel
        self.tree.grid(column=0, row=0, sticky=tk.NSEW)
        self.tree.grid_rowconfigure(0, weight=1)
        self.tree.grid_columnconfigure(0, weight=1)

        self.vsb.grid(column=1, row=0, sticky=tk.NSEW)
        self.vsb.grid_rowconfigure(0, weight=1)
        self.vsb.grid_columnconfigure(1, weight=1)

        self.hsb.grid(column=0, row=1, columnspan=2, sticky=tk.NSEW)
        self.hsb.grid_rowconfigure(1, weight=1)
        self.hsb.grid_columnconfigure(0, weight=1)

    def load_labels(self):
        self.tree.heading("#0", text=resources.get_labels('Navigation', 'dir_structure'))
        self.tree.heading("size", text=resources.get_labels('Navigation', 'file_size'))

    def populate_tree(self, tree, node):
        if tree.set(node, "type") != 'directory':
            return

        path = tree.set(node, "fullpath")
        tree.delete(*tree.get_children(node))

        parent = tree.parent(node)
        special_dirs = [] if parent else glob.glob('.') + glob.glob('..')

        for p in special_dirs + os.listdir(path):
            ptype = None
            p = os.path.join(path, p).replace('\\', '/')
            if os.path.isdir(p):
                ptype = "directory"
            elif os.path.isfile(p):
                ptype = "file"

            fname = os.path.split(p)[1]
            id = tree.insert(node, "end", text=fname, values=[p, ptype])

            if ptype == 'directory':
                if fname not in ('.', '..'):
                    tree.insert(id, 0, text="dummy")
                    tree.item(id, text=fname)
            elif ptype == 'file':
                size = os.stat(p).st_size
                tree.set(id, "size", "%d bytes" % size)

    def populate_roots(self, tree):
        global INITIAL_PATH
        dir = os.path.abspath('.').replace('\\', '/')
        if INITIAL_PATH is None:
            INITIAL_PATH = dir
        node = tree.insert('', 'end', text=dir, values=[dir, "directory"])
        self.populate_tree(tree, node)

    def update_tree(self, event):
        tree = event.widget
        self.populate_tree(tree, tree.focus())

    def change_dir(self, event):
        tree = event.widget
        node = tree.focus()
        if tree.parent(node):
            path = os.path.abspath(tree.set(node, "fullpath"))
            if os.path.isdir(path):
                os.chdir(path)
                tree.delete(tree.get_children(''))
                self.populate_roots(tree)
                if INITIAL_PATH is not None:
                    os.chdir(INITIAL_PATH)
            elif os.path.isfile(path):
                self.main_instance.return_class_instance('editor_instance').open_file(path)

    def autoscroll(self, sbar, first, last):
        """Hide and show scrollbar as needed."""
        first, last = float(first), float(last)
        if first <= 0 and last >= 1:
            sbar.grid_remove()
        else:
            sbar.grid()
        sbar.set(first, last)
