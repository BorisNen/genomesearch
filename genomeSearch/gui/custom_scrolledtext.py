"""
GenomeSearch
    Copyright (C) 2020  BorisNen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
# ======================
# imports
# ======================
import tkinter as tk
from tkinter import scrolledtext, ttk, filedialog
import genomeSearch.gui.custom_notebook as notebook
import genomeSearch.resources as resources
import genomeSearch.gui.messagebox as msg
import ntpath
import os.path as os_path


class CustomScrolledText(scrolledtext.ScrolledText):
    def __init__(self, master=None, **kw):
        self.frame_instance = master

        scrolledtext.ScrolledText.__init__(self, master, **kw)
        self.line_number = tk.IntVar()
        self.line_number.set(1)

        self.cursor_info_bar = ttk.Label(self, text='Line: 1 | Column: 1')
        self.cursor_info_bar.pack(expand='no', fill=None, side='right', anchor='se')

        self.load_text_are_info()

    def load_text_are_info(self):
        self.bind_key_actions()
        # set up the pop-up menu
        self.right_click_menu()

    def bind_key_actions(self):
        self.bind('<Control-N>', self.get_editor_instance().confirm_add)
        self.bind('<Control-n>', self.get_editor_instance().confirm_add)
        self.bind('<Control-O>', self.get_editor_instance().open_file_dialog)
        self.bind('<Control-o>', self.get_editor_instance().open_file_dialog)
        self.bind('<Control-S>', self.save)
        self.bind('<Control-s>', self.save)
        self.bind('<Control-Shift-S>', self.save_as)
        self.bind('<Control-Shift-s>', self.save_as)
        self.bind('<Control-f>', self.find_text)
        self.bind('<Control-F>', self.find_text)
        self.bind('<Control-A>', self.select_all)
        self.bind('<Control-a>', self.select_all)
        self.bind('<Control-y>', self.redo)
        self.bind('<Control-Y>', self.redo)
        self.bind('<Control-z>', self.undo)
        self.bind('<Control-Z>', self.undo)
        self.bind('<Any-KeyPress>', self.on_content_changed)
        self.bind('<Button-1>', self.on_content_changed)
        self.tag_configure('active_line', background='ivory2')

    def right_click_menu(self):
        self.popup_menu = tk.Menu(self, tearoff=0)
        self.popup_menu.add_command(label=resources.get_labels('Editor', 'cut'), compound='left', command=self.cut)
        self.popup_menu.add_command(label=resources.get_labels('Editor', 'copy'), compound='left', command=self.copy)
        self.popup_menu.add_command(label=resources.get_labels('Editor', 'paste'), compound='left', command=self.paste)
        self.popup_menu.add_command(label=resources.get_labels('Editor', 'undo'), compound='left', command=self.undo)
        self.popup_menu.add_command(label=resources.get_labels('Editor', 'redo'), compound='left', command=self.redo)
        self.popup_menu.add_separator()
        self.popup_menu.add_command(label=resources.get_labels('Editor', 'select_all'), underline=7, command=self.select_all)
        self.bind('<Button-3>', self.show_popup_menu)
        self.focus_set()

    def save(self, event=None):
        tab_name = self.get_editor_instance().return_current_tab_name()

        for key, tab in notebook.OPENED_FILES.items():
            if tab_name in key and self.frame_instance == tab:
                path = key
                self.write_to_file(path)
                break
        else:
            self.save_as()

        return "break"

    def write_to_file(self, path):
        try:
            content = self.get(1.0, 'end')
            with open(path, 'w') as file:
                file.write(content)
                self.get_editor_instance().request_instance('log_instance').log_info(resources.get_labels('Editor', 'saved_file')+' '+path)
        except IOError:
            msg.warning(resources.get_labels('MessageBox', 'msg_save_error_title'),
                        resources.get_labels('MessageBox', 'msg_save_error'))

    def save_as(self, event=None):
        input_file_name = filedialog.asksaveasfilename(defaultextension=".txt",
                                                               filetypes=[("All Files", "*.*"),
                                                                          ("Text Documents", "*.txt")])
        if input_file_name:
            input_file_name = os_path.abspath(input_file_name)
            # notebook.OPENED_FILES[input_file_name] = self.get_current_tab()
            # self.tab_control.tab(self.tab_control.select(), text=self.filename_from_path(input_file_name))
            self.write_to_file(input_file_name)
            self.get_editor_instance().add_new_tab(self.filename_from_path(input_file_name), open(input_file_name), input_file_name)
        return "break"

    def find_text(self, event=None):
        search_toplevel = tk.Toplevel(self.frame_instance)
        search_toplevel.title(resources.get_labels('Editor', 'find_text'))
        search_toplevel.transient(self.frame_instance)

        tk.Label(search_toplevel, text=resources.get_labels('Editor', 'find_text')).grid(row=0, column=0, sticky='e')

        search_entry_widget = tk.Entry(
            search_toplevel, width=25)
        search_entry_widget.grid(row=0, column=1, padx=2, pady=2, sticky='we')
        search_entry_widget.focus_set()
        ignore_case_value = tk.IntVar()
        tk.Checkbutton(search_toplevel, text=resources.get_labels('Editor', 'ignore_case'), variable=ignore_case_value).grid(
            row=1, column=1, sticky='e', padx=2, pady=2)
        tk.Button(search_toplevel, text=resources.get_labels('Editor', 'find_all'), underline=0,
               command=lambda: self.search_output(
                   search_entry_widget.get(), ignore_case_value.get(),
                   search_toplevel, search_entry_widget)
               ).grid(row=0, column=2, sticky='e' + 'w', padx=2, pady=2)

    def search_output(self, needle, if_ignore_case,
                      search_toplevel, search_box):
        self.tag_remove('match', '1.0', tk.END)
        matches_found = 0
        if needle:
            start_pos = '1.0'
            while True:
                start_pos = self.search(needle, start_pos,
                                                nocase=if_ignore_case, stopindex=tk.END)
                if not start_pos:
                    break
                end_pos = '{}+{}c'.format(start_pos, len(needle))
                self.tag_add('match', start_pos, end_pos)
                matches_found += 1
                start_pos = end_pos
                self.tag_config(
                'match', foreground='red', background='yellow')
        search_box.focus_set()
        search_toplevel.title('{} matches found'.format(matches_found))

    def select_all(self, event=None):
        self.tag_add('sel', '1.0', 'end')
        return "break"

    def redo(self, event=None):
        self.event_generate("<<Redo>>")
        self.on_content_changed()
        return 'break'

    def undo(self, event=None):
        self.event_generate("<<Undo>>")
        self.on_content_changed()
        return "break"

    def cut(self):
        self.event_generate("<<Cut>>")
        self.on_content_changed()
        return "break"

    def copy(self):
        self.event_generate("<<Copy>>")
        return "break"

    def paste(self):
        self.event_generate("<<Paste>>")
        self.on_content_changed()
        return "break"

    def show_popup_menu(self, event):
        self.popup_menu.tk_popup(event.x_root, event.y_root)

    def on_content_changed(self, event=None):
        self.update_line_numbers()
        self.update_cursor_info_bar()

    def update_cursor_info_bar(self, event=None):
        row, col = self.index(tk.INSERT).split('.')
        line_num, col_num = str(int(row)), str(int(col) + 1)  # col starts at 0
        infotext = "Line: {0} | Column: {1}".format(line_num, col_num)
        self.cursor_info_bar.config(text=infotext)

    def highlight_line(self, interval=100):
        self.tag_remove("active_line", 1.0, "end")
        self.tag_add(
            "active_line", "insert linestart", "insert lineend+1c")
        self.after(interval, self.toggle_highlight)

    def undo_highlight(self):
        self.tag_remove("active_line", 1.0, "end")

    def toggle_highlight(self, event=None):
        #if to_highlight_line.get():
        self.highlight_line()
        #else:
        #   self.undo_highlight()

    def update_line_numbers(self, event=None):
        line_numbers = self.get_line_numbers()
        self.frame_instance.line_number_bar.config(state='normal')
        self.frame_instance.line_number_bar.delete('1.0', 'end')
        self.frame_instance.line_number_bar.insert('1.0', line_numbers)
        self.frame_instance.line_number_bar.config(state='disabled')

    def get_line_numbers(self):
        output = ''
        if self.line_number.get():
            row, col = self.index("end").split('.')
            for i in range(1, int(row)):
                output += str(i) + '\n'
        return output

    def get_editor_instance(self):
        return self.frame_instance.return_master()

    def filename_from_path(self, path):
        head, tail = ntpath.split(path)
        return tail or ntpath.basename(head)
