"""
GenomeSearch
    Copyright (C) 2020  BorisNen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import tkinter as tk
import datetime
from tkinter import scrolledtext


class Log:
    def __init__(self, parent):
        self.text_area = scrolledtext.ScrolledText(parent, wrap=tk.WORD)
        self.text_area.pack(fill=tk.BOTH, expand=True)
        self.text_area.insert(tk.INSERT, '<'+datetime.datetime.now().strftime('%y-%m-%d %H:%M:%S')+'>' +
                              ' GenomeSearch started!')
        self.text_area.config(state='disabled')

    def log_info(self, text):
        self.text_area.config(state='normal')
        self.text_area.insert(tk.END, '\n'+'<'+datetime.datetime.now().strftime('%y-%m-%d %H:%M:%S')+'>' + ' ' + text)
        self.text_area.config(state='disabled')
        self.text_area.yview(tk.END)
