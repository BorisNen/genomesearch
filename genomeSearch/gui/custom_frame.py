"""
GenomeSearch
    Copyright (C) 2020  BorisNen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
# ======================
# imports
# ======================
import tkinter as tk
from tkinter import ttk


class CustomFrame(ttk.Frame):
    def __init__(self, editor_instance, master=None, **kw):
        self.editor_instance = editor_instance
        ttk.Frame.__init__(self, master, **kw)

        self.line_number_bar = tk.Text(self, width=4, padx=3, takefocus=0, border=0,
                               background='LightGrey', state='disabled', wrap='none')
        self.line_number_bar.pack(side='left', fill='y')

    def return_master(self):
        return self.editor_instance
