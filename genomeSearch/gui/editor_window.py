"""
GenomeSearch
    Copyright (C) 2020  BorisNen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
# ======================
# imports
# ======================
import tkinter as tk
from tkinter import filedialog, ttk
import genomeSearch.resources as resources
import genomeSearch.gui.custom_notebook as notebook
import genomeSearch.gui.custom_scrolledtext as cust_scr_text
import genomeSearch.gui.custom_frame as custom_frame
import genomeSearch.gui.popup as popup
import genomeSearch.gui.messagebox as msg
import genomeSearch.gui.navigation_window as nav
import genomeSearch.parsers.entrez_parse as parser
import ntpath
import os.path as os_path

# ======================
# Global Constants
# ======================
show_cursor_info = None


class Editor:
    def __init__(self, parent, main_instance):
        self.tab_control = notebook.CustomNotebook(parent)
        self.main_window = main_instance

        # Set globals
        global show_cursor_info
        show_cursor_info = tk.IntVar()
        show_cursor_info.set(1)

        # Create initial tab
        tab1 = custom_frame.CustomFrame(self, self.tab_control)
        self.tab_control.add(tab1, text=resources.get_labels('Editor', 'tab_initial'))

        # -----------------------------------------------------
        text_area = cust_scr_text.CustomScrolledText(tab1, wrap=tk.NONE, undo=1)
        text_area.pack(fill=tk.BOTH, expand=True)

        # Pack to make visible
        self.tab_control.pack(fill=tk.BOTH, expand=True)

    def load_labels(self):
        # not used anymore
        self.tab_control.add(self.get_current_tab(), text=resources.get_labels('Editor', 'tab_initial'))

    def get_all_tabs(self):
        tabs = []
        for tab in self.tab_control.children.values():
            tabs.append(tab)
        return tabs

    def get_current_tab(self):
        return self.tab_control.children.get(self.tab_control.select()[50:])

    def get_all_text_areas(self):
        text_areas = []
        for tab in self.tab_control.children.values():
            text_areas.append(tab.winfo_children().__getitem__(1).winfo_children().__getitem__(1))
        return text_areas
    
    def get_current_text_area(self):
        return self.tab_control.children.get(self.tab_control.select()[50:]).winfo_children().__getitem__(1).winfo_children().__getitem__(1)

    def is_opened_file(self, path):
        for key in notebook.OPENED_FILES.keys():
            if path is not None and path == key:
                return True

    def popup_add_button(self):
        name = self.new_entry.get()
        if not name:
            msg.warning(resources.get_labels('MessageBox', 'msg_title_error'),
                        resources.get_labels('Editor', 'err_message'))
        else:
            self.add_new_tab(name, None, None)
            self.instance.destroy()

    def confirm_add(self, event=None):
        self.instance = popup.Popup('360x150', resources.get_labels('Editor', 'popup_title'))
        window = self.instance.return_popup()

        new_label = ttk.Label(window, text=resources.get_labels('Editor', 'popup_name') + " ")
        new_label.grid(row=0, column=2, sticky="W", padx=5, pady=5)

        self.new_entry = ttk.Entry(window)
        self.new_entry.grid(row=0, column=3, sticky="W", padx=5, pady=5)

        add_button = ttk.Button(window, text=resources.get_labels('Editor', 'popup_confirm'),
                                command=self.popup_add_button)
        add_button.grid(row=0, column=4, sticky="W", padx=5, pady=5)

    def add_new_tab(self, name, file, path):
        name = self.filename_from_path(name)
        if self.is_opened_file(path):
            self.tab_control.select(notebook.OPENED_FILES.get(path))
        else:
            tab_name = custom_frame.CustomFrame(self, self.tab_control)
            self.tab_control.add(tab_name, text=name)

            text_area = cust_scr_text.CustomScrolledText(tab_name, wrap=tk.NONE, undo=1)
            text_area.pack(fill=tk.BOTH, expand=True)

            self.tab_control.select(tab_name)

            if file is not None and path is not None:
                notebook.OPENED_FILES[path] = tab_name
                text_area.insert(tk.END, file.read())

            self.show_cursor_info_bar()

    def show_cursor_info_bar(self):
        global show_cursor_info
        # show_cursor_info_checked = self.get_current_text_area().cursor_info.get()
        if show_cursor_info.get():
            for text_area in self.get_all_text_areas():
                text_area.cursor_info_bar.pack(expand='no', fill=None, side='right', anchor='se')
        else:
            for text_area in self.get_all_text_areas():
                text_area.cursor_info_bar.pack_forget()

    def open_file_dialog(self, event=None):
        input_file_name = filedialog.askopenfilename(defaultextension=".txt",
                                                             filetypes=[("All Files", "*.*"),
                                                                        ("Text Documents", "*.txt")])

        input_file_name = os_path.abspath(input_file_name)
        self.open_file(input_file_name)

    def open_file(self, path):
        for ext_format in nav.ALLOWED_EXTENSIONS:
            if path.lower().endswith(ext_format):
                parser.open_file(path[path.rfind('\\') + 1:], path, self)

    def request_instance(self, name):
        return self.main_window.return_class_instance(name)

    def return_current_tab_name(self):
        return self.tab_control.tab(self.tab_control.select(), "text")

    def filename_from_path(self, path):
        head, tail = ntpath.split(path)
        return tail or ntpath.basename(head)
