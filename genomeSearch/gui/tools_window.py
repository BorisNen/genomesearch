"""
GenomeSearch
    Copyright (C) 2020  BorisNen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
# ======================
# imports
# ======================
import tkinter as tk
from tkinter import ttk, filedialog as fd
from os import path
import genomeSearch.resources as resources
import genomeSearch.gui.popup as popup
import genomeSearch.machineLearning.ml_controller as ml
import genomeSearch.parsers.fasta_parse as fparse
from PIL import ImageTk, Image

# ======================
# Global Constants
# ======================
ICONS = ('new_file', 'open_file', 'save', 'cut', 'copy', 'paste',
         'undo', 'redo', 'find_text')


class Tools:
    def __init__(self, parent, main_instance):
        self.main_instance = main_instance

        self.tool_container = ttk.LabelFrame(parent)
        self.tool_container.pack(fill=tk.BOTH, expand=True)

        self.pca = ttk.LabelFrame(self.tool_container, text='PCA')
        self.pca.pack(fill=tk.BOTH, expand=True)

        self.mlp = ttk.LabelFrame(self.tool_container, text='MLP')
        self.mlp.pack(fill=tk.BOTH, expand=True)

        # Class bound holders for the MLP Classifier options
        # Holds class : count for the read dataset
        self.classes_dict = {}
        # Holds id : class for the user display
        self.predict_dict = {}
        self.neuron_layers = (100, 50, 25)
        self.activation_function = 'relu'
        self.backpropagation = 'adam'
        self.l2_penalty = 0.0001
        self.early_stopping = True
        self.predict_type = 3

        self.setup_pca()
        self.setup_mlp()

        self.load_labels()

    def load_labels(self):
        self.browse_file_pca.configure(text=resources.get_labels('Tools', 'npy_file_label'))
        self.tool_container.configure(text=resources.get_labels('Tools', 'tools_title'))
        self.browse_folder_pca.configure(text=resources.get_labels('Popup', 'directory_label'))
        self.file_name_label_pca.configure(text=resources.get_labels('Popup', 'file_name_label'))
        self.components_pca.configure(text=resources.get_labels('Tools', 'components_pca'))
        self.submit_pca.configure(text=resources.get_labels('Tools', 'perform_pca'))
        self.add_button.configure(text=resources.get_labels('Tools', 'tools_pca_dataset'))
        self.specify_classes_button.configure(text=resources.get_labels('Tools', 'specify_classes'))
        self.check_brca.configure(text=resources.get_labels('Tools', 'classify_unknown'))
        self.check_train.configure(text=resources.get_labels('Tools', 'train_checkbox'))
        self.check_predict.configure(text=resources.get_labels('Tools', 'predict_checkbox'))
        self.browse_file_mlp.configure(text=resources.get_labels('Tools', 'npy_file_label'))
        self.browse_folder_mlp.configure(text=resources.get_labels('Popup', 'directory_label'))
        self.browse_file_predict_mlp.configure(text=resources.get_labels('Tools', 'joblib_file_label'))
        self.file_name_label_mlp.configure(text=resources.get_labels('Popup', 'file_name_label'))
        self.submit_mlp_train.configure(text=resources.get_labels('Tools', 'train_mlp'))
        self.pca.configure(text=resources.get_labels('Tools', 'pca_labelframe'))
        self.mlp.configure(text=resources.get_labels('Tools', 'mlp_labelframe'))

    def setup_pca(self):
        self.browse_file_pca = ttk.Button(self.pca, command=lambda: self.open_file_dialog(pathEntryPca, pathLenPca, 'npy'))
        self.browse_file_pca.grid(column=0, row=1, padx=2, pady=8, sticky=tk.NSEW)

        file_location_pca = tk.StringVar()
        pathLenPca = 20
        pathEntryPca = ttk.Entry(self.pca, width=pathLenPca, textvariable=file_location_pca)
        pathEntryPca.grid(column=1, row=1, padx=2, pady=8, sticky=tk.NSEW)

        self.browse_folder_pca = ttk.Button(self.pca, command=lambda: self.browse_directory(self.pca, fileEntryPca, fileLenPca))
        self.browse_folder_pca.grid(column=0, row=2, padx=2, pady=8, sticky=tk.NSEW)

        save_location_pca = tk.StringVar()
        fileLenPca = 20
        fileEntryPca = ttk.Entry(self.pca, width=fileLenPca, textvariable=save_location_pca)
        fileEntryPca.grid(column=1, row=2, padx=2, pady=8, sticky=tk.NSEW)

        self.file_name_label_pca = ttk.Label(self.pca)
        self.file_name_label_pca.grid(column=0, row=3, padx=2, pady=8, sticky=tk.NE)

        save_name_pca = tk.StringVar()
        fileNamePca = ttk.Entry(self.pca, width=30, textvariable=save_name_pca)
        fileNamePca.grid(column=1, row=3, padx=3, pady=8, sticky=tk.NSEW)

        self.components_pca = ttk.Label(self.pca)
        self.components_pca.grid(column=0, row=4, padx=2, pady=8, sticky=tk.NE)

        spin = tk.Spinbox(self.pca, from_=1, to=59, width=5, bd=6)
        spin.grid(column=1, row=4, sticky=tk.W)

        self.submit_pca = ttk.Button(self.pca, command=lambda: ml.run_pca(self.main_instance.return_class_instance('log_instance'),
                                                       pathEntryPca.get(),
                                                       int(spin.get()),
                                                       fileEntryPca.get(),
                                                       fileNamePca.get()))
        self.submit_pca.grid(column=0, row=5, padx=2, pady=8, sticky=tk.NSEW)

        self.add_button = ttk.Button(self.pca, command=self.dataset_window)
        self.add_button.grid(column=1, row=5, padx=2, pady=8, sticky=tk.NSEW)

        self.specify_classes_button = ttk.Button(self.pca, command=self.specify_classes)
        self.specify_classes_button.grid(column=2, row=5, padx=2, pady=8, sticky=tk.NSEW)

    def setup_mlp(self):
        self.radVar = tk.IntVar()
        self.check_brca = ttk.Radiobutton(self.mlp, variable=self.radVar, value=1, command=self.checkCallback)
        self.check_brca.grid(column=0, row=0, sticky=tk.W)

        self.check_train = ttk.Radiobutton(self.mlp, variable=self.radVar, value=2, command=self.checkCallback)
        self.check_train.grid(column=1, row=0, sticky=tk.W)

        self.check_predict = ttk.Radiobutton(self.mlp, variable=self.radVar, value=3, command=self.checkCallback)
        self.check_predict.grid(column=2, row=0, sticky=tk.W)

        self.browse_file_mlp = ttk.Button(self.mlp, command=lambda: self.open_file_dialog(self.pathEntryMlp, pathLenMlp, 'npy'))
        self.browse_file_mlp.grid(column=0, row=2, padx=2, pady=8, sticky=tk.NSEW)

        file_location_mlp = tk.StringVar()
        pathLenMlp = 20
        self.pathEntryMlp = ttk.Entry(self.mlp, width=pathLenMlp, textvariable=file_location_mlp)
        self.pathEntryMlp.grid(column=1, row=2, padx=2, pady=8, sticky=tk.NSEW)

        self.browse_folder_mlp = ttk.Button(self.mlp, command=lambda: self.browse_directory(self.mlp, self.fileEntryMlp,
                                                                                            fileLenMlp))
        self.browse_folder_mlp.grid(column=0, row=3, padx=2, pady=8, sticky=tk.NSEW)

        self.browse_file_predict_mlp = ttk.Button(self.mlp, command=lambda: self.open_file_dialog(self.fileEntryMlp,
                                                                                                  fileLenMlp, 'joblib'))
        self.browse_file_predict_mlp.grid(column=0, row=3, padx=2, pady=8, sticky=tk.NSEW)


        save_location_mlp = tk.StringVar()
        fileLenMlp = 20
        self.fileEntryMlp = ttk.Entry(self.mlp, width=fileLenMlp, textvariable=save_location_mlp)
        self.fileEntryMlp.grid(column=1, row=3, padx=2, pady=8, sticky=tk.NSEW)

        self.file_name_label_mlp = ttk.Label(self.mlp)
        self.file_name_label_mlp.grid(column=0, row=4, padx=2, pady=8, sticky=tk.NE)

        save_name_mlp = tk.StringVar()
        self.fileNameMlp = ttk.Entry(self.mlp, width=30, textvariable=save_name_mlp)
        self.fileNameMlp.grid(column=1, row=4, padx=3, pady=8, sticky=tk.NSEW)

        # components_mlp = ttk.Label(self.mlp,
        #                             text=resources.get_labels('Tools', 'components_mlp'))
        # components_mlp.grid(column=0, row=5, padx=2, pady=8, sticky=tk.NE)

        # self.spin = tk.Spinbox(self.mlp, from_=1, to=100, width=5, bd=6)
        # self.spin.grid(column=1, row=5, sticky=tk.W)

        self.submit_mlp_train = ttk.Button(self.mlp, command=lambda: ml.train_classifier(
            self.main_instance.return_class_instance('log_instance'),
            self.backpropagation,
            self.activation_function,
            self.l2_penalty,
            self.early_stopping,
            self.neuron_layers,
            self.fileEntryMlp.get() + '/' + self.fileNameMlp.get(),
            self.pathEntryMlp.get(),
            self.classes_dict))
        self.submit_mlp_train.grid(column=0, row=5, padx=2, pady=8, sticky=tk.NSEW)

        self.submit_mlp_predict = ttk.Button(self.mlp, command=lambda: ml.predict_classifier(
            self.main_instance.return_class_instance('log_instance'),
            self.pathEntryMlp.get(),
            self.fileEntryMlp.get(),
            self.classes_dict,
            self.predict_dict,
            self.predict_type))
        self.submit_mlp_predict.grid(column=1, row=5, padx=2, pady=8, sticky=tk.NSEW)
        self.submit_mlp_predict.configure(text=resources.get_labels('Tools', 'predict_mlp'))

        self.open_mlp_config = ttk.Button(self.mlp, command=self.open_mlp_configuration)
        self.open_mlp_config.grid(column=2, row=5, padx=2, pady=8, sticky=tk.NSEW)
        self.open_mlp_config.configure(text=resources.get_labels('Tools', 'config_mlp'))

        self.check_train.invoke()

    # GUI Callback function
    def checkCallback(self, *ignoredArgs):
        # if self.radVar.get() == 1:
        #     self.browse_file_predict_mlp.grid_remove()
        #     self.browse_folder_mlp.grid()
        #     self.browse_folder_mlp.configure(state='disabled')
        #     self.pathEntryMlp.delete(0, 'end')
        #     self.pathEntryMlp.configure(state='disabled')
        #     self.fileEntryMlp.delete(0, 'end')
        #     self.fileEntryMlp.configure(state='disabled')
        #     self.fileNameMlp.delete(0, 'end')
        #     self.fileNameMlp.configure(state='disabled')
        #     # self.spin.configure(state='disabled')
        #     self.submit_mlp_train.configure(state='disabled')
        #     self.submit_mlp_predict.configure(state='normal')
        #     self.submit_mlp_predict.configure(text=resources.get_labels('Tools', 'predict_brca'))
        if self.radVar.get() == 1:
            self.same_fields()
            self.predict_type = 1
        elif self.radVar.get() == 2:
            self.fileEntryMlp.delete(0, 'end')
            self.fileEntryMlp.configure(state='disabled')
            self.browse_folder_mlp.configure(state='normal')
            self.pathEntryMlp.configure(state='normal')
            self.fileEntryMlp.configure(state='normal')
            self.fileNameMlp.configure(state='normal')
            # self.spin.configure(state='normal')
            self.submit_mlp_train.configure(state='normal')
            self.browse_file_predict_mlp.grid_remove()
            self.browse_folder_mlp.grid()
            self.submit_mlp_predict.configure(state='disabled')
        else:
            self.same_fields()
            self.predict_type = 3

    def same_fields(self):
        self.browse_file_predict_mlp.grid()
        self.browse_folder_mlp.grid_remove()
        self.pathEntryMlp.configure(state='normal')
        self.fileEntryMlp.delete(0, 'end')
        self.fileNameMlp.delete(0, 'end')
        self.fileNameMlp.configure(state='disabled')
        # self.spin.configure(state='disabled')
        self.submit_mlp_train.configure(state='disabled')
        self.submit_mlp_predict.configure(state='normal')
        self.submit_mlp_predict.configure(text=resources.get_labels('Tools', 'predict_mlp'))

    def dataset_window(self):
        dataset_popup = popup.Popup('720x320',
                                  resources.get_labels('Popup', 'title_ml'))
        self.dataset_window_instance = dataset_popup.return_popup()

        browse_file = ttk.Button(self.dataset_window_instance, text=resources.get_labels('Popup', 'file_label'),
                            command=lambda: self.open_file_dialog(pathEntry, pathLen, ''))
        browse_file.grid(column=0, row=0, padx=2, pady=8, sticky=tk.NSEW)

        file_location = tk.StringVar()
        pathLen = 20
        pathEntry = ttk.Entry(self.dataset_window_instance, width=pathLen, textvariable=file_location)
        pathEntry.grid(column=1, row=0, padx=2, pady=8, sticky=tk.NSEW)

        browse_folder = ttk.Button(self.dataset_window_instance, text=resources.get_labels('Popup', 'directory_label'),
                            command=lambda: self.browse_directory(self.dataset_window_instance, fileEntry, fileLen))
        browse_folder.grid(column=0, row=1, padx=2, pady=8, sticky=tk.NSEW)

        save_location = tk.StringVar()
        fileLen = 20
        fileEntry = ttk.Entry(self.dataset_window_instance, width=fileLen, textvariable=save_location)
        fileEntry.grid(column=1, row=1, padx=2, pady=8, sticky=tk.NSEW)

        file_name_label = ttk.Label(self.dataset_window_instance,
                                    text=resources.get_labels('Popup', 'file_name_label'))
        file_name_label.grid(column=0, row=2, padx=2, pady=8, sticky=tk.NE)

        save_name = tk.StringVar()
        fileName = ttk.Entry(self.dataset_window_instance, width=30, textvariable=save_name)
        fileName.grid(column=1, row=2, padx=2, pady=8, sticky=tk.NSEW)

        label_spin = ttk.Label(self.dataset_window_instance,
                                   text=resources.get_labels('Popup', 'longest_sequence_input'))
        label_spin.grid(column=0, row=3, padx=2, pady=8, sticky=tk.NSEW)

        spin = tk.Spinbox(self.dataset_window_instance, from_=0, to=100000, width=8, bd=6)
        spin.grid(column=1, row=3, padx=2, pady=8, sticky=tk.NSEW)

        submit = ttk.Button(self.dataset_window_instance,
                            text=resources.get_labels('Popup', 'button_generate'),
                            command=lambda: self.generate_dataset(int(spin.get()), pathEntry, fileEntry, fileName))
        submit.grid(column=0, row=4, padx=2, pady=8, sticky=tk.NSEW)

    def generate_dataset(self, longest_sequence_spin, pathEntry, fileEntry, fileName):
        fparse.dict_convert_multiple_fasta(longest_sequence_spin, pathEntry.get(), fileEntry.get(), fileName.get(),
                                           self.main_instance.return_class_instance('log_instance'))
        self.dataset_window_instance.destroy()

    def browse_directory(self, parent, entry, variable):
        fDir = path.dirname(__file__)
        fName = fd.askdirectory(parent=parent, initialdir=fDir)
        entry.config(state='enabled')
        entry.delete(0, tk.END)
        entry.insert(0, fName)

        if len(fName) > variable:
            entry.config(width=len(fName) + 3)

    def open_file_dialog(self, entry, variable, file_type, event=None):
        if file_type == 'npy':
            input_file_name = fd.askopenfilename(defaultextension=".npy",
                                                                 filetypes=[("NPY File", "*.npy")])
        elif file_type == 'joblib':
            input_file_name = fd.askopenfilename(defaultextension=".npy",
                                                 filetypes=[("JOBLIB File", "*.joblib")])
        else:
            input_file_name = fd.askopenfilename(defaultextension=".fasta",
                                                                 filetypes=[("FASTA sequence file", "*.fasta")])

        input_file_name = path.abspath(input_file_name)
        entry.config(state='enabled')
        entry.delete(0, tk.END)
        entry.insert(0, input_file_name)

        if len(input_file_name) > variable:
            entry.config(width=len(input_file_name) + 3)

    def specify_classes(self):
        specify_popup = popup.Popup('720x320',
                                  resources.get_labels('Tools', 'specify_classes'))
        specify_popup_instance = specify_popup.return_popup()

        all_entries = []

        def add_box():
            frame = ttk.Frame(specify_popup_instance)
            frame.pack()

            ttk.Label(frame, text=resources.get_labels('Popup', 'specify_classes_label')).grid(row=0, column=0)

            ent1 = ttk.Entry(frame)
            ent1.grid(row=1, column=0)

            ttk.Label(frame, text=resources.get_labels('Popup', 'specify_classes_data_count')).grid(row=0, column=1)

            ent2 = ttk.Entry(frame)
            ent2.grid(row=1, column=1)

            all_entries.append((ent1, ent2))

        def show_entries():
            self.classes_dict = {}
            for number, (ent1, ent2) in enumerate(all_entries):
                self.classes_dict.__setitem__(ent1.get(), int(ent2.get()))
                self.predict_dict.__setitem__(number, ent1.get())
            specify_popup_instance.destroy()

        showButton = ttk.Button(specify_popup_instance, text=resources.get_labels('Popup', 'save_and_exit'), command=show_entries)
        showButton.pack()

        addboxButton = ttk.Button(specify_popup_instance, text=resources.get_labels('Popup', 'new_data_label'), command=add_box)
        addboxButton.pack()

        frame_for_boxes = ttk.Frame(specify_popup_instance)
        frame_for_boxes.pack()

    def open_mlp_configuration(self):
        query_popup = popup.Popup('420x420',
                                  resources.get_labels('Tools', 'config_mlp'))
        query_instance = query_popup.return_popup()

        layers = []

        def add_box():
            frame = ttk.Frame(query_instance)
            frame.pack()

            ttk.Label(frame, text=resources.get_labels('Popup', 'mlp_config_neurons')).grid(row=0, column=0)

            ent1 = ttk.Entry(frame)
            ent1.grid(row=1, column=0)

            layers.append(ent1)

        def show_entries():
            self.activation_function = acrivation.get()
            self.backpropagation = backprop.get()
            self.l2_penalty = float(penalty.get())
            self.early_stopping = self.checkVar.get()
            arr = []
            for number, (ent1) in enumerate(layers):
                arr.append(int(ent1.get()))
            self.neuron_layers = tuple(arr)
            query_instance.destroy()

        showButton = ttk.Button(query_instance, text=resources.get_labels('Popup', 'save_and_exit'), command=show_entries)
        showButton.pack()

        database_label = ttk.Label(query_instance,
                                   text=resources.get_labels('Popup', 'mlp_config_activation'))
        database_label.pack()
        # -----------------------------------------------------
        acrivation = tk.StringVar()
        acrivation_chosen = ttk.Combobox(query_instance, width=25, textvariable=acrivation, state='readonly')
        values = []
        values.append("relu")
        values.append("tanh")
        values.append("logistic")
        values.append("identity")
        acrivation_chosen['values'] = values
        acrivation_chosen.pack()
        acrivation_chosen.current(0)

        backprop_label = ttk.Label(query_instance,
                                   text=resources.get_labels('Popup', 'mlp_config_backpropagation'))
        backprop_label.pack()
        # -----------------------------------------------------
        backprop = tk.StringVar()
        backprop_chosen = ttk.Combobox(query_instance, width=25, textvariable=backprop, state='readonly')
        backprop_values = []
        backprop_values.append("adam")
        backprop_values.append("sgd")
        backprop_values.append("lbfgs")
        backprop_chosen['values'] = backprop_values
        backprop_chosen.pack()
        backprop_chosen.current(0)

        l2 = ttk.Label(query_instance,
                                    text="L2 penalty (0.0001): ")
        l2.pack()

        # spin = tk.Spinbox(query_instance, from_=0.0001, to=1, width=5, bd=6)
        # spin.pack()

        penalty = ttk.Entry(query_instance)
        penalty.pack()

        early = ttk.Label(query_instance,
                                    text=resources.get_labels('Popup', 'mlp_config_early_stopping'))
        early.pack()

        self.checkVar= tk.BooleanVar()
        check = tk.Checkbutton(query_instance, var=self.checkVar)
        check.pack()
        check.select()

        addboxButton = ttk.Button(query_instance, text=resources.get_labels('Popup', 'mlp_config_add_new_layer'), command=add_box)
        addboxButton.pack()
