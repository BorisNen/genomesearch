# -*- mode: python -*-

block_cipher = None
added_files = [
         ( 'genomeSearch\\resources\\Bulgarian.properties','resources' ),
		 ( 'genomeSearch\\resources\\English.properties','resources' ),
		 ( 'genomeSearch\\resources\\German.properties', 'resources'),
         ( 'genomeSearch\\resources\\icons\\Logo.ico', 'resources\\icons' ),
		 ( 'genomeSearch\\resources\\icons\\about.ico', 'resources\\icons' ),
		 ( 'genomeSearch\\resources\\icons\\language.ico', 'resources\\icons' ),
		 ( 'genomeSearch\\resources\\icons\\copy.ico', 'resources\\icons' ),
		 ( 'genomeSearch\\resources\\icons\\cut.ico', 'resources\\icons' ),
		 ( 'genomeSearch\\resources\\icons\\find_text.ico', 'resources\\icons' ),
		 ( 'genomeSearch\\resources\\icons\\new_file.ico', 'resources\\icons' ),
		 ( 'genomeSearch\\resources\\icons\\open.ico', 'resources\\icons' ),
		 ( 'genomeSearch\\resources\\icons\\paste.ico', 'resources\\icons' ),
		 ( 'genomeSearch\\resources\\icons\\redo.ico', 'resources\\icons' ),
		 ( 'genomeSearch\\resources\\icons\\remote.ico', 'resources\\icons' ),
		 ( 'genomeSearch\\resources\\icons\\save.ico', 'resources\\icons' ),
		 ( 'genomeSearch\\resources\\icons\\save_as_icon.ico', 'resources\\icons' ),
		 ( 'genomeSearch\\resources\\icons\\undo.ico', 'resources\\icons' )
         ]

a = Analysis(['genomeSearch\\main_window.py'],
             pathex=['genomeSearch\\gui'],
             binaries=[],
             datas=added_files,
             hiddenimports=['sklearn', 'sklearn.utils._cython_blas', 'requests'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='main_window',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True,
		  icon='genomeSearch\\resources\\icons\\Logo.ico')
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='main_window')
