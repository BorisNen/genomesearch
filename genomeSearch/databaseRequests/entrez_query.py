'''
GenomeSearch
    Copyright (C) 2020  BorisNen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

# ======================
# imports
# ======================
import tkinter as tk
from tkinter import ttk, messagebox, filedialog as fd
from os import path

import requests
import genomeSearch.gui.popup as popup
import genomeSearch.resources as resources
import genomeSearch.parsers.entrez_parse as parser
import genomeSearch.gui.messagebox as msg
import asyncio

# ======================
# Global Constants
# ======================
DATABASES = {
    "Entrez Nucletotides": "nucleotide"
}
CONST_URL = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/'
SUCCESS_CODES = [200, 201, 202, 203, 204, 205, 206, 207, 208, 226]


class Entrez:
    def __init__(self, main_window):
        self.main_window = main_window
        query_popup = popup.Popup('720x320',
                                  resources.get_labels('Popup', 'database_title'))
        self.query_instance = query_popup.return_popup()
        self.create_widgets()

    def create_widgets(self):
        # Widget Frame -----------------------------------------------------
        content_holder = ttk.LabelFrame(self.query_instance)
        content_holder.grid(column=0, row=0, sticky=tk.NSEW)

        # Radio buttons for the search term -----------------------------------------------------
        self.var = tk.StringVar()
        radio_button = ttk.Radiobutton(content_holder, text=resources.get_labels('Popup', 'term_label'),
                                       variable=self.var, value='term')
        radio_button.grid(column=0, row=0, sticky=tk.W, padx=2, pady=8)
        # -----------------------------------------------------
        radio_button = ttk.Radiobutton(content_holder, text=resources.get_labels('Popup', 'id_label'),
                                       variable=self.var, value='id')
        radio_button.grid(column=1, row=0, sticky=tk.W, padx=2, pady=8)

        # Label and field for entering search term -----------------------------------------------------
        id_term_label = ttk.Label(content_holder,
                                  text=resources.get_labels('Popup', 'id_term_label'))
        id_term_label.grid(column=0, row=1, padx=2, pady=8, sticky=tk.W)
        # -----------------------------------------------------
        self.id_term = tk.StringVar()
        enter_id_term = ttk.Entry(content_holder, textvariable=self.id_term)
        enter_id_term.grid(column=1, row=1, padx=2, pady=8, sticky=tk.W)

        # Label and field for number of result id's -----------------------------------------------------
        id_num_label = ttk.Label(content_holder,
                                  text=resources.get_labels('Popup', 'id_number'))
        id_num_label.grid(column=0, row=2, padx=2, pady=8, sticky=tk.W)
        # -----------------------------------------------------
        self.id_number = tk.IntVar()
        enter_id_number = ttk.Entry(content_holder, textvariable=self.id_number)
        enter_id_number.grid(column=1, row=2, padx=2, pady=8, sticky=tk.W)
        self.id_number.set(20)

        # Label and combobox for choosing database -----------------------------------------------------
        database_label = ttk.Label(content_holder,
                                   text=resources.get_labels('Popup', 'database_label'))
        database_label.grid(column=0, row=3, padx=2, pady=8, sticky=tk.W)
        # -----------------------------------------------------
        self.database = tk.StringVar()
        database_chosen = ttk.Combobox(content_holder, width=25, textvariable=self.database, state='readonly')
        values = []
        for key in DATABASES:
            values.append(key)
        database_chosen['values'] = values
        database_chosen.grid(column=1, row=3, padx=2, pady=8, sticky=tk.W)
        database_chosen.current(0)

        # Label and field for choosing directory for saving -----------------------------------------------------
        directory_label = ttk.Label(content_holder,
                                    text=resources.get_labels('Popup', 'directory_label'))
        directory_label.grid(column=0, row=4, padx=2, pady=8, sticky=tk.W)

        # Add Widgets to Manage Files Frame -----------------------------------------------------
        file = tk.StringVar()
        self.entryLen = 20
        self.fileEntry = ttk.Entry(content_holder, width=self.entryLen, textvariable=file)
        self.fileEntry.grid(column=1, row=4, padx=2, pady=8, sticky=tk.W)

        # Button for browsing directories -----------------------------------------------------
        browse = ttk.Button(content_holder, text="...", command=self.browse_directory)
        browse.grid(column=2, row=4, padx=2, pady=8)

        # Button for sending the search query -----------------------------------------------------
        submit = ttk.Button(content_holder,
                            text=resources.get_labels('Popup', 'button_submit'),
                            command=self.build_query)
        submit.grid(column=0, row=5, padx=2, pady=8, sticky=tk.W)

    def browse_directory(self):
        fDir = path.dirname(__file__)
        fName = fd.askdirectory(parent=self.query_instance, initialdir=fDir)
        self.fileEntry.config(state='enabled')
        self.fileEntry.delete(0, tk.END)
        self.fileEntry.insert(0, fName)

        if len(fName) > self.entryLen:
            self.fileEntry.config(width=len(fName) + 3)

    def check_empty_fields(self):
        if not self.var.get() or not self.id_term.get() or not self.fileEntry.get():
            return True

    def build_query(self):
        try:
            if self.check_empty_fields():
                msg.warning(resources.get_labels('MessageBox', 'msg_title_error'),
                            resources.get_labels('MessageBox', 'msg_database_form'))
            elif self.id_number.get() > 10000:
                msg.warning(resources.get_labels('MessageBox', 'msg_title_error'),
                            resources.get_labels('MessageBox', 'msg_id_number_form'))
            else:
                global CONST_URL
                self.save_name = self.fileEntry.get() + '/'+self.id_term.get()

                request_type = ''
                search_type = self.var.get()
                if search_type == 'term':
                    self.save_name = self.save_name + '.xml'
                    request_type = 'esearch.fcgi'
                    params = {'db': DATABASES.get(self.database.get()), 'term': self.id_term.get(),
                              'retmax': self.id_number.get()}
                else:
                    self.save_name = self.save_name + '.fasta'
                    request_type = 'efetch.fcgi'
                    params = {'db': DATABASES.get(self.database.get()), 'id': self.id_term.get(), 'rettype': 'fasta',
                              'retmode':'text'}

                url = CONST_URL + request_type

                loop = asyncio.get_event_loop()
                loop.run_until_complete(self.send_async_request(url, params, search_type))
        except tk.TclError:
            msg.warning(resources.get_labels('MessageBox', 'msg_title_error'),
                        resources.get_labels('MessageBox', 'msg_id_number_form'))

    async def send_async_request(self, url, params, search_type):
        loop = asyncio.get_event_loop()
        response = loop.run_in_executor(None, requests.get, url, params)

        try:
            r = await asyncio.wait_for(response, timeout=10)
        except asyncio.TimeoutError:
            self.main_window.return_class_instance('log_instance').log_info(resources.get_labels('Popup', 'search_timeout'))
        except requests.exceptions.ConnectionError:
            self.main_window.return_class_instance('log_instance').log_info(resources.get_labels('Popup', 'search_no_connection'))
        else:
            if r.status_code in SUCCESS_CODES:
                file = open(self.save_name, "wb").write(r.content)
                if search_type == 'term':
                    self.log_result()
                    self.main_window.return_class_instance('log_instance').log_info(resources.get_labels('Popup', 'search_result') +
                                         ' ' + self.save_name)
            else:
                self.main_window.return_class_instance('log_instance').log_info(resources.get_labels('Popup', 'search_error') +
                                         ' ' + self.save_name)
        finally:
            self.query_instance.destroy()

    def log_result(self):
        file = open(self.save_name)
        result_array = parser.parse_xml_result_ids(file)
        if not result_array:
            self.main_window.return_class_instance('log_instance').log_info(resources.get_labels('Popup', 'search_no_id_found'))
        else:
            self.main_window.return_class_instance('log_instance').log_info(resources.get_labels('Popup', 'search_found_id') +
                                     '\n' + '\n'.join(result_array))
