"""
GenomeSearch
    Copyright (C) 2020  BorisNen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
# ======================
# imports
# ======================
import numpy as np
import math
import genomeSearch.resources as resources
import genomeSearch.machineLearning.pca_compression as pca

# ======================
# Global Constants
# ======================
dict_word = {}
BASES = {
    1: 'A',
    2: 'T',
    3: 'C',
    4: 'G'
}
ACCEPTABLE_LETTERS = ['A', 'T', 'C', 'G']


def generate_dict(log_window):
    log_window.log_info(resources.get_labels('Parser', 'generate_dictionary'))
    counter = 1
    for a in range(1, 5):
        dict_word.__setitem__(BASES.get(a), counter)
        counter += 1
        for b in range(1, 5):
            dict_word.__setitem__(BASES.get(a)+BASES.get(b), counter)
            counter += 1
            for c in range(1, 5):
                dict_word.__setitem__(BASES.get(a)+BASES.get(b) + BASES.get(c), counter)
                counter += 1
                for d in range(1, 5):
                    dict_word.__setitem__(BASES.get(a)+BASES.get(b) + BASES.get(c) + BASES.get(d), counter)
                    counter += 1
                    for e in range(1, 5):
                        dict_word.__setitem__(BASES.get(a)+BASES.get(b) + BASES.get(c) + BASES.get(d) + BASES.get(e), counter)
                        counter += 1
    log_window.log_info(resources.get_labels('Parser', 'generate_dictionary_done'))


def find_longest_sequence(file, log_window):
    log_window.log_info(resources.get_labels('Parser', 'longest_sequence'))
    sequence_counter = 0
    longest = 0
    with open(file) as f:
        sequence_length_counter = 0
        for line in f:
            if line == '\n':
                if sequence_length_counter > longest:
                    longest = sequence_length_counter
                sequence_length_counter = 0
            if not line.startswith('>'):
                for letter in line:
                    if letter == '\n':
                        break
                    if letter not in ACCEPTABLE_LETTERS:
                        continue
                    sequence_length_counter += 1
            else:
                sequence_counter += 1

    log_window.log_info(str(resources.get_labels('Parser', 'longest_sequence_done')).format(sequence_counter, longest))
    return longest


# Working converter
def dict_convert_multiple_fasta(longest_sequence_spin, file, save_path, save_name, log_window):
    log_window.log_info(resources.get_labels('Parser', 'dict_multiple_fasta'))
    generate_dict(log_window)

    if longest_sequence_spin is not None \
            and longest_sequence_spin != '' \
            and longest_sequence_spin != 0:
        longest = longest_sequence_spin
    else:
        longest = find_longest_sequence(file, log_window)

    target_dimension = math.ceil(longest / 5)
    dataset = []
    sequence_counter = 0
    with open(file) as f:
        sequence = []
        for line in f:
            if line == '\n':
                zeros = target_dimension - len(sequence)
                while zeros != 0:
                    sequence.append(0)
                    zeros -= 1
                dataset.append(np.asarray(sequence))
                sequence = []
                sequence_counter += 1
            if not line.startswith('>'):
                cur_word = ''
                counter = 0
                for letter in line:
                    if letter == '\n':
                        break
                    if letter not in ACCEPTABLE_LETTERS:
                        continue
                    counter += 1
                    cur_word += letter
                    if counter == 5:
                        sequence.append(dict_word.get(cur_word))
                        cur_word = ''
                        counter = 0
                if counter > 0:
                    sequence.append(dict_word.get(cur_word))
                    cur_word = ''
                    counter = 0

    '''
    zeros = target_dimension - math.ceil(sequence_length_counter / 5)
    while zeros != 0:
        sequence.append(0)
        zeros -= 1
    '''
    # temp = len(dataset)
    # if temp < 59:
    #     pca.LENGTH = temp
    #     array = []
    #     for x in range(59-temp):
    #         target = []
    #         for y in range(len(dataset[0])):
    #             target.append(0)
    #         dataset.append(target)
    # else:
    #     pca.LENGTH = 0

    dataset = np.asarray(dataset)
    np.save(save_path + '/' + save_name + '.npy', dataset)
    log_window.log_info(str(resources.get_labels('Parser', 'dict_multiple_fasta_done')).format(save_path, save_name))


# Example, not used
def convert_multiple_fasta(file, save_path, save_name, log_window):
    log_window.log_info(resources.get_labels('Parser', 'dict_multiple_fasta'))
    longest = find_longest_sequence(file, log_window)
    target_dimension = math.ceil(longest * 4)
    dataset = []
    sequence_counter = 1
    with open(file) as f:
        sequence = []
        sequence_length_counter = 0
        for line in f:
            if line == '\n':
                zeros = target_dimension - sequence_length_counter * 4
                while zeros != 0:
                    sequence.append(0)
                    zeros -= 1
                dataset.append(np.asarray(sequence))
                sequence = []
                sequence_counter += 1
                sequence_length_counter = 0
            if not line.startswith('>'):
                for letter in line:
                    if letter == 'A':
                        sequence.append(1)
                        sequence.append(0)
                        sequence.append(0)
                        sequence.append(0)
                        sequence_length_counter += 1
                    elif letter == 'T':
                        sequence.append(0)
                        sequence.append(1)
                        sequence.append(0)
                        sequence.append(0)
                        sequence_length_counter += 1
                    elif letter == 'C':
                        sequence.append(0)
                        sequence.append(0)
                        sequence.append(1)
                        sequence.append(0)
                        sequence_length_counter += 1
                    elif letter == 'G':
                        sequence.append(0)
                        sequence.append(0)
                        sequence.append(0)
                        sequence.append(1)
                        sequence_length_counter += 1
                    elif letter == '\n':
                        break

    zeros = target_dimension - sequence_length_counter
    while zeros != 0:
        sequence.append(0)
        zeros -= 1
    dataset = np.asarray(dataset)
    np.save(save_path + '/' + save_name + '.npy', dataset)
    log_window.log_info(str(resources.get_labels('Parser', 'dict_multiple_fasta_done')).format(save_path, save_name))
