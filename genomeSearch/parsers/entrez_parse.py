"""
GenomeSearch
    Copyright (C) 2020  BorisNen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import xml.etree.ElementTree as parser


def parse_xml_result_ids(xml_object):
    id_array = []
    tree = parser.parse(xml_object)
    root = tree.getroot()
    for node in root.iter('IdList'):
        for child in node:
            id_array.append(child.text)
    return id_array


def open_file(name, path, editor_instance): # TODO change with parser for each file extension
    editor_instance.add_new_tab(name, open(path), path)
