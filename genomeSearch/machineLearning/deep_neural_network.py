"""
GenomeSearch
    Copyright (C) 2020  BorisNen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
# ======================
# imports
# ======================
from sklearn.metrics import accuracy_score
from sklearn.neural_network import MLPClassifier
from joblib import dump, load
import genomeSearch.resources as resources

HIDDEN_LAYERS = {}


def mlp_classifier_train(log_window_instance, solver, activation, alpha, early_stopping, neuron_layers, location, X_train, y_train):
    clf = MLPClassifier(solver=solver, activation=activation, alpha=alpha, learning_rate='adaptive',
                        hidden_layer_sizes=neuron_layers, random_state=1, early_stopping=early_stopping)
    clf.fit(X_train, y_train)
    dump(clf, location+'.joblib')
    log_window_instance.log_info(str(resources.get_labels('MLController', 'ml_model_trained') + location))


def mlp_classifier_predict(log_window_instance, classes_dict, file, X_test, y_test):
    clf = load(file)
    y_predicted = clf.predict(X_test)
    arr_orig = []
    arr_pred = []
    for elem in y_test:
        arr_orig.append(classes_dict.get(elem))
    for elem in y_predicted:
        arr_pred.append(classes_dict.get(elem))
    # print('Test classes: ')
    # print(arr_orig)
    log_window_instance.log_info(str(resources.get_labels('MLController', 'ml_predicted_classes')))
    log_window_instance.log_info(', '.join(arr_pred))
    log_window_instance.log_info(resources.get_labels('MLController', 'ml_model_accuracy').format(accuracy_score(y_test, y_predicted)))


def mlp_classifier_predict_unknown(log_window_instance, classes_dict, file, X_test):
    clf = load(file)
    y_predicted = clf.predict(X_test)
    arr_pred = []
    for elem in y_predicted:
        arr_pred.append(classes_dict.get(elem))
    # print('Test classes: ')
    # print(arr_orig)
    log_window_instance.log_info(str(resources.get_labels('MLController', 'ml_predicted_classes')))
    log_window_instance.log_info(', '.join(arr_pred))

