"""
GenomeSearch
    Copyright (C) 2020  BorisNen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
# ======================
# imports
# ======================
import genomeSearch.machineLearning.deep_neural_network as dnn
import genomeSearch.machineLearning.pca_compression as pca
import numpy as np
import genomeSearch.resources as resources

# ======================
# Global Constants
# ======================
# Removed on 06.12.2020 - everything is dynamic now
# BRCA_CLASSES = {
#     0:"Mutiertes BRCA1 Gen",
#     1:"Mutiertes BRCA2 Gen",
#     2:"Gesundes BRCA1 Gen",
#     3:"Gesundes BRCA2 Gen"
# }
# DEFAULT_MODEL_PATH = './BRCA.ckpt.meta'


def run_pca(log_window_instance, data, component_number, save_path, save_name):
    error = pca.pca_compression(data, component_number, save_path, save_name)
    log_window_instance.log_info(
        str(resources.get_labels('Parser', 'dict_multiple_fasta_done')).format(save_path, save_name))
    log_window_instance.log_info(
        str(resources.get_labels('MLController', 'pca_error')).format((error/682)*100))


def train_classifier(log_window_instance, solver, activation, alpha, early_stopping, neuron_layers, location, file, classes_dict):
    X_train, y_train = read_dataset(file, classes_dict)
    dnn.mlp_classifier_train(log_window_instance, solver, activation, alpha, early_stopping, neuron_layers, location, X_train, y_train)


def predict_classifier(log_window_instance, datasetFile, modelFile, classes_dict, predict_dict, predict_type):
    if predict_type == 1:
        X_test = read_unknown(datasetFile)
        dnn.mlp_classifier_predict_unknown(log_window_instance, predict_dict, modelFile, X_test)
    else:
        X_test, y_test = read_dataset(datasetFile, classes_dict)
        dnn.mlp_classifier_predict(log_window_instance, predict_dict, modelFile, X_test, y_test)


def read_dataset(file, classes_dict=None):
    if classes_dict is None:
        classes_dict = {}
    dataset = np.load(file)
    y_part = []
    values = list(classes_dict.values())
    for number, key in enumerate(classes_dict.keys()):
        for num in range(values[number]):
            y_part.append(number)

    X_part = dataset[:]
    return X_part, y_part


def read_unknown(file):
    dataset = np.load(file)
    return dataset[:]
