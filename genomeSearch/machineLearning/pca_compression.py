"""
GenomeSearch
    Copyright (C) 2020  BorisNen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
# ======================
# imports
# ======================
from sklearn.decomposition import PCA
import numpy as np

LENGTH = 0


def pca_compression(data, component_number, save_path, save_name):
    dataset = np.load(data, allow_pickle=True)
    pca = PCA(n_components=component_number)
    x_transformed = pca.fit_transform(dataset)
    x_inversed = pca.inverse_transform(x_transformed)
    error = np.mean(np.sum(np.square(x_inversed - dataset), axis=1))
    # if LENGTH == 0:
    x_transformed = np.asarray(x_transformed)
    # else:
    #     x_transformed = np.asarray(x_transformed[:LENGTH])
    np.save(save_path + '/' + save_name + '.npy', x_transformed)

    return error
